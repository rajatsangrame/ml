import os
import glob
import pandas as pd
import xml.etree.ElementTree as ET
import tensorflow as tf
import numpy as np

ground_truth = {}
tf_detection = {}
score = 0.000
count = 0


def bbox_overlap_iou(bboxes1, bboxes2):
    """
    Args:
        bboxes1: shape (total_bboxes1, 4)
            with x1, y1, x2, y2 point order.
        bboxes2: shape (total_bboxes2, 4)
            with x1, y1, x2, y2 point order.
        p1 *-----
           |     |
           |_____* p2
    Returns:
        Tensor with shape (total_bboxes1, total_bboxes2)
        with the IoU (intersection over union) of bboxes1[i] and bboxes2[j]
        in [i, j].
    """

    x11, y11, x12, y12 = tf.split(bboxes1, 4, axis=1)
    x21, y21, x22, y22 = tf.split(bboxes2, 4, axis=1)

    xI1 = tf.maximum(x11, tf.transpose(x21))
    yI1 = tf.maximum(y11, tf.transpose(y21))

    xI2 = tf.minimum(x12, tf.transpose(x22))
    yI2 = tf.minimum(y12, tf.transpose(y22))

    inter_area = (xI2 - xI1 + 1) * (yI2 - yI1 + 1)

    bboxes1_area = (x12 - x11 + 1) * (y12 - y11 + 1)
    bboxes2_area = (x22 - x21 + 1) * (y22 - y21 + 1)

    union = (bboxes1_area + tf.transpose(bboxes2_area)) - inter_area

    return tf.maximum(inter_area / union, 0)


for directory in ['ground_truth', 'tf_detection']:
    for xml_file in glob.glob('/home/rajat/two/{}/*.xml'.format(directory)):
        temp_dict = {}
        tree = ET.parse(xml_file)
        root = tree.getroot()
        for member in root.findall('object'):
            temp_dict[member[0].text] = [int(member[4][0].text), int(member[4][1].text), int(member[4][2].text),
                                         int(member[4][3].text)]

        xml = os.path.basename(xml_file)
        if directory == 'ground_truth':
            ground_truth[os.path.splitext(xml)[0]] = temp_dict
        elif directory == 'tf_detection':
            tf_detection[os.path.splitext(xml)[0]] = temp_dict

print(ground_truth)
print('-------------------------------------------------')
print(tf_detection)

bbox1 = tf.placeholder(tf.float32)
bbox2 = tf.placeholder(tf.float32)
overlap_op = bbox_overlap_iou(bbox1, bbox2)

with tf.Session() as sess:
    bbox1_value = [0, 0, 0, 0]
    bbox2_value = [0, 0, 0, 0]

    print("-------------------------------------------------")
    print("Session Started...")

    for key, value in ground_truth.items():
        print("Evaluating for {} ..".format(key))
        for temp_key, temp_value in value.items():
            # print("Groud truth of {} is {}".format(temp_key,temp_value))
            bbox1_value = temp_value
            try:
                bbox2_value = tf_detection[key][temp_key]
            except KeyError:
                print("KEY ERROR")
                pass

            bbox1_val = [bbox1_value]
            bbox2_val = [bbox2_value]

            overlap = sess.run(overlap_op, feed_dict={
                bbox1: np.array(bbox1_val),
                bbox2: np.array(bbox2_val),
            })

            score = score + overlap[0]
            count = count + 1
            # print(overlap[0])

print("Accuracy is {}".format(score / count))
