"""
Usage:
  # From tensorflow/models/
  # Create train data:
  python3 generate_tfrecord.py --csv_input=data/train_labels.csv  --output_path=data/train.record

  # Create test data:
  python3 generate_tfrecord.py --csv_input=data/test_labels.csv  --output_path=data/test.record
"""
from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

import os
import io
import pandas as pd
import tensorflow as tf

from PIL import Image
from object_detection.utils import dataset_util
from collections import namedtuple, OrderedDict

flags = tf.app.flags
flags.DEFINE_string('csv_input', '', 'Path to the CSV input')
flags.DEFINE_string('output_path', '', 'Path to output TFRecord')
flags.DEFINE_string('image_dir', '', 'Path to images')
FLAGS = flags.FLAGS


# TO-DO replace this with label map
def class_text_to_int(row_label):
    if row_label == 'Mahindra':
        return 66
    elif row_label == 'White':
        return 1
    elif row_label == '2 axle':
        return 2
    elif row_label == 'Plate':
        return 3
    elif row_label == 'U':
        return 4
    elif row_label == 'P':
        return 5
    elif row_label == '1':
        return 6
    elif row_label == '6':
        return 7
    elif row_label == 'C':
        return 8
    elif row_label == 'A':
        return 9
    elif row_label == '4':
        return 10
    elif row_label == '7':
        return 11
    elif row_label == '0':
        return 12
    elif row_label == '2':
        return 13
    elif row_label == 'Hyundai':
        return 14
    elif row_label == 'D':
        return 15
    elif row_label == 'L':
        return 16
    elif row_label == '3':
        return 17
    elif row_label == 'B':
        return 18
    elif row_label == 'M':
        return 19
    elif row_label == '9':
        return 20
    elif row_label == 'car':
        return 21
    elif row_label == 'plate':
        return 22
    elif row_label == 'Toyota':
        return 23
    elif row_label == 'Silver':
        return 24
    elif row_label == 'H':
        return 25
    elif row_label == 'E':
        return 26
    elif row_label == '5':
        return 27
    elif row_label == 'Ford':
        return 28
    elif row_label == 'G':
        return 29
    elif row_label == '8':
        return 30
    elif row_label == 'J':
        return 31
    elif row_label == 'Honda':
        return 32
    elif row_label == 'Red':
        return 33
    elif row_label == 'R':
        return 34
    elif row_label == 'K':
        return 35
    elif row_label == 'Y':
        return 36
    elif row_label == 'Z':
        return 37
    elif row_label == 'Maruti Suzuki':
        return 38
    elif row_label == 'Renault':
        return 39
    elif row_label == 'Green':
        return 40
    elif row_label == 'V':
        return 41
    elif row_label == 'Skoda':
        return 42
    elif row_label == 'Gray or Grey':
        return 43
    elif row_label == 'Suzuki':
        return 44
    elif row_label == 'S':
        return 45
    elif row_label == 'Volkswagen':
        return 46
    elif row_label == 'N':
        return 47
    elif row_label == 'F':
        return 48
    elif row_label == 'T':
        return 49
    elif row_label == 'W':
        return 50
    elif row_label == 'Black':
        return 51
    elif row_label == 'Brown':
        return 52
    elif row_label == 'Audi':
        return 53
    elif row_label == 'X':
        return 54
    elif row_label == 'Jeep':
        return 55
    elif row_label == 'Q':
        return 56
    elif row_label == 'TATA':
        return 57
    elif row_label == 'Orange':
        return 58
    elif row_label == 'Nissan':
        return 59
    elif row_label == 'Magenta':
        return 60
    elif row_label == 'O':
        return 61
    elif row_label == '1 axle':
        return 62
    elif row_label == 'Tan':
        return 63
    elif row_label == 'Blue':
        return 64
    elif row_label == 'Maroon':
        return 65
    else:
        None


def split(df, group):
    data = namedtuple('data', ['filename', 'object'])
    gb = df.groupby(group)
    return [data(filename, gb.get_group(x)) for filename, x in zip(gb.groups.keys(), gb.groups)]


def create_tf_example(group, path):
    with tf.gfile.GFile(os.path.join(path, '{}'.format(group.filename)), 'rb') as fid:
        encoded_jpg = fid.read()
    encoded_jpg_io = io.BytesIO(encoded_jpg)
    image = Image.open(encoded_jpg_io)
    width, height = image.size

    filename = group.filename.encode('utf8')
    image_format = b'jpg'
    xmins = []
    xmaxs = []
    ymins = []
    ymaxs = []
    classes_text = []
    classes = []

    for index, row in group.object.iterrows():
        xmins.append(row['xmin'] / width)
        xmaxs.append(row['xmax'] / width)
        ymins.append(row['ymin'] / height)
        ymaxs.append(row['ymax'] / height)
        classes_text.append(row['class'].encode('utf8'))
        classes.append(class_text_to_int(row['class']))

    tf_example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/filename': dataset_util.bytes_feature(filename),
        'image/source_id': dataset_util.bytes_feature(filename),
        'image/encoded': dataset_util.bytes_feature(encoded_jpg),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))
    return tf_example


def main(_):
    writer = tf.python_io.TFRecordWriter(FLAGS.output_path)
    path = os.path.join(FLAGS.image_dir)
    examples = pd.read_csv(FLAGS.csv_input)
    grouped = split(examples, 'filename')
    for group in grouped:
        tf_example = create_tf_example(group, path)
        writer.write(tf_example.SerializeToString())

    writer.close()
    output_path = os.path.join(os.getcwd(), FLAGS.output_path)
    print('Successfully created the TFRecords: {}'.format(output_path))


if __name__ == '__main__':
    tf.app.run()
