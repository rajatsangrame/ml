import os
import glob
import pandas as pd
import xml.etree.ElementTree as ET


def xml_to_csv(path):
    xml_list = []
    for xml_file in glob.glob(path + '/*.xml'):
        tree = ET.parse(xml_file)
        root = tree.getroot()
        print(xml_file)
        # import pdb; pdb.set_trace()
        for member in root.findall('object'):
            filename_jpg = root.find('filename').text.replace('.webp', '.jpg')

            value = (filename_jpg,
                     int(root.find('size')[0].text),
                     int(root.find('size')[1].text),
                     member[0].text,
                     int(member[4][0].text),
                     int(member[4][1].text),
                     int(member[4][2].text),
                     int(member[4][3].text)
                     )
        #     import pdb; pdb.set_trace()
            xml_list.append(value)
    column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
    xml_df = pd.DataFrame(xml_list, columns=column_name)
#     import pdb; pdb.set_trace()
    return xml_df


def main():
    for directory in ['test','train']:
        #image_path = os.path.join(os.getcwd(), 'images/{}'.format(directory))
        image_path = '/home/rajat/Downloads/img/{}'.format(directory)

        # import pdb; pdb.set_trace()
        xml_df = xml_to_csv(image_path)
        # import pdb; pdb.set_trace()
        xml_df.to_csv('/home/rajat/ml2/models/research/object_detection/data/{}_labels.csv'.format(directory), index=None)
        print('Successfully converted xml to csv.')

main()

